
kubectl apply -f ns\ns-be.yaml
kubectl apply -f ns\ns-db.yaml
kubectl apply -f ns\ns-mn.yaml

kubectl apply -f redis\redis-config.yaml  
kubectl apply -f redis\redis-deployment.yaml


kubectl apply -f be\be-configmap.yaml -n ns-be
kubectl apply -f be\modified\be1.yaml  -n ns-be

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/prometheus --wait
kubectl apply -f monitoring\pm-configmap.yaml -n ns-mn
kubectl apply -f monitoring\pm-deployment.yaml -n ns-mn


# after installation components you will be able to see services loadbalancer IPs by following command

# kubectl get all  -n ns-be  
# kubectl get all  -n ns-mn

# helm install elasticsearch elastic/elasticsearch  -f values.yaml --namespace=ns-lg  --create-namespace --wait
# helm install kibana elastic/kibana --namespace=ns-lg --wait
# helm install filebeat elastic/filebeat --namespace=ns-lg --wait

