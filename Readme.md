## home assingment

This project contains an application and manifest folder which represent the front application and underlying infrastructure declared with YAML format.

### Front application

Display weather information for 3 cities. 

- Data integration with WatherAPI
    - Environment variables  - API KEY (I use the same key for all environments)
    - Integration Job (Job is time running job, when the application is started it will bring data from Weather API to Redis DB)
    - Updated time is stored in Redis will be shown on the page when data is updated on the front application.
- Redis for in-memory database
    - There is no persistence volume attached to the pod.
    - Redis exposed by Cluster IP __internally__. 
- Prometheus
    - Redis metrics exported Prometheus by Redis-exporter sidecar container
    - application metrics available /metrics endpoint yet not integrated with Prometheus. 
    - I wanted to use Prometheus for metrics elk for application logging purposes 

- Db Connection
    - Application knows the Redis service name provided by the ConfigMap component. 
    - I used file mapping to enable the application 

- Logging 
    - For logging purposes, I initiated the ELK stack on my cluster. 
    - The idea is to collect all metrics in the cluster and individually from metrics by components (database and application)
### for the purpose of separation app/Db/logging components I have to create 3 namespaces
### no RBAC implementation
### please run the following file in powershell if you use windows/ not just copy and paste to terminal. 
`> manifest\init.ps1`