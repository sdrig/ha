

$version = "v" + 37

docker build -t be:$version .

docker tag be:$version sadrix/be:$version
$image = "sadrix/be:$version"

Write-Host $image -ForegroundColor Red

docker push $image
Write-Host (Get-Location) -ForegroundColor Red
Set-Location -Path "..\..\manifest"
Write-Host (Get-Location) -ForegroundColor yellow

((Get-Content -path "be.yaml" -Raw) -replace '{{VERSION}}',$version) | Set-Content -Path ".\modified\be1.yaml"


kubectl create ns ns-db
kubectl create ns ns-be

kubectl apply -f redis\redis-config.yaml  
kubectl apply -f redis\redis-deployment.yaml

kubectl apply -f be-configmap.yaml -n ns-be
kubectl apply -f be.yaml  -n ns-be

kubectl apply -f be-configmap.yaml -n ns-be
kubectl apply -f ".\modified\be1.yaml"  -n ns-be

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
