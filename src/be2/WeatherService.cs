﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Net.Http;
namespace be2
{
    public class WeatherService : IHostedService
    {
        private int executionCount = 0;
        private readonly ILogger<WeatherService> _logger;
        private HttpClient _httpClient;
        private readonly IDistributedCache _distributedCache2;
        private readonly IConfiguration _configuration;
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly IDatabaseAsync _databaseAsync;
        private Timer _timer = null!;
        private readonly string _apiKey = string.Empty;
        private readonly string _redisConnectionString = string.Empty;
        public WeatherService(ILogger<WeatherService> logger,
            HttpClient client,
            IDistributedCache distributedCache,
            IConfiguration configuration,
            IConnectionMultiplexer connectionMultiplexer)
        {
            _logger = logger;
            ///_distributedCache = distributedCache;
            _configuration = configuration;
            this._connectionMultiplexer = connectionMultiplexer;
            _httpClient = client;
            _apiKey = Environment.GetEnvironmentVariable("Weather_ApiKey") ?? configuration["Weather_ApiKey"];
            _redisConnectionString = Environment.GetEnvironmentVariable("Redis_Connection_String") ?? configuration["Redis_Connection_String"];
            _databaseAsync = connectionMultiplexer.GetDatabase();
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await ExecuteAsync(cancellationToken);
            _ = Task.FromResult(1);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
        protected async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Demo Service is starting");
            _logger.LogInformation($"_redisConnectionString: {_redisConnectionString}");
            stoppingToken.Register(() => _logger.LogInformation("Demo Service is stopping."));
            var cities = new List<string> {
                "Riga",
                "Tallinn",
                "Moscow"
            };
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("http://api.openweathermap.org");
            foreach (var city in cities)
            {
                var response = await _httpClient.GetAsync($"/data/2.5/weather?q={city}&appid={_apiKey}&units=metric");
                response.EnsureSuccessStatusCode();
                var stringResult = await response.Content.ReadAsStringAsync();
                await _databaseAsync.StringSetAsync(city, stringResult);
                var rawWeather = JsonConvert.DeserializeObject<OpenWeatherResponse>(stringResult);
            }
            await _databaseAsync.StringSetAsync("updated", DateTime.UtcNow.ToString());
            _logger.LogDebug("Demo service is stopping");
        }
    }

    public record OpenWeatherResponse
    {
        public string Name { get; set; }

        public IEnumerable<WeatherDescription> Weather { get; set; }

        public Main Main { get; set; }
    }

    public record WeatherDescription
    {
        public string Main { get; set; }
        public string Description { get; set; }
    }

    public record Main
    {
        public string Temp { get; set; }
    }

}
