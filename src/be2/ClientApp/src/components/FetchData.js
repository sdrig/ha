import React, { Component } from 'react';

export class FetchData extends Component {
    static displayName = FetchData.name;

    constructor(props) {
        super(props);
        this.state = { forecasts: [], loading: true, updated:"" };
    }

    componentDidMount() {
        this.populateWeatherData();
    }
    static renderUpdate(updated) {
        return (
            <p>This table updated in Redis at <b>{updated}</b></p>
        );
    }
    static renderForecastsTable(forecasts) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Country</th>
                        <th>Date</th>
                        <th>Temp. (C)</th>
                        <th>Feels Like</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {console.log(forecasts)}
                    {forecasts.map(forecast =>
                        <tr key={forecast.name}>
                            <td>{forecast.name}</td>
                            <td>{forecast.sys.country}</td>
                            <td>{forecast.dt}</td>
                            <td>{forecast.main.temp}</td>
                            <td>{forecast.main.feels_like}</td>
                            <td>{forecast.weather[0].description}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : FetchData.renderForecastsTable(this.state.forecasts);
        let date = this.state.loading ? ""
            : FetchData.renderUpdate(this.state.updated);
        return (
            <div>
                <h5 id="tabelLabel" >data loaded from redis. is only accessible in cluster with service name.</h5>
                <p>{date}</p>
                {contents}
            </div>
        );
    }

    async populateWeatherData() {
        debugger;
        const response1 = await fetch('weatherforecast/w/Riga');
        const response2 = await fetch('weatherforecast/w/Tallinn');
        const response3 = await fetch('weatherforecast/w/Moscow');
        const updated =   await fetch('weatherforecast/w/updated');
        const data1 = await response1.json();
        const data2 = await response2.json();
        const data3 = await response3.json();
        var data = [data1, data2, data3];
        var upd = await updated.text();
        this.setState({ forecasts: data, loading: false, updated: `${upd}` });
    }
}
