using be2;
using Prometheus;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);
builder
    .Configuration//.SetBasePath(Directory.GetCurrentDirectory())
    .AddEnvironmentVariables()
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", optional: true, reloadOnChange: false)
    .AddJsonFile("appsettings.k8s.json", optional: true, reloadOnChange: false);

// Add services to the container.
builder.Services.AddHostedService<WeatherService>();
builder.Services.AddHttpClient();
var redisConnectionKey = "Redis_Connection_String";
var redisConnectionValue = builder.Configuration[redisConnectionKey] ?? Environment.GetEnvironmentVariable(redisConnectionKey);

builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = redisConnectionValue + ":6379";
});
ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(
            new ConfigurationOptions
            {
                EndPoints = {
                    $"{redisConnectionValue}:6379"
                }
            });
builder.Services.AddSingleton<IConnectionMultiplexer>(redis);

builder.Services.AddControllersWithViews();
builder
    .Configuration//.SetBasePath(Directory.GetCurrentDirectory())
    .AddEnvironmentVariables()
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", optional: true, reloadOnChange: false)
    .AddJsonFile("appsettings.k8s.json", optional: true, reloadOnChange: false);
builder.Services.AddSingleton<IConfigurationRoot>((IConfigurationRoot)builder.Configuration);
builder.Services.AddSingleton<IConfiguration>(builder.Configuration);
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
}

app.UseStaticFiles();
app.UseRouting();

var counter = Metrics.CreateCounter("api_request_count", "Counts requests to the Weather API endpoints",
    new CounterConfiguration
    {
        LabelNames = new[] { "method", "endpoint" }
    });
app.Use((context, next) =>
{
    counter.WithLabels(context.Request.Method, context.Request.Path).Inc();
    return next();
});

app.UseMetricServer("/metrics");

app.UseHttpMetrics();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");


app.MapFallbackToFile("index.html"); ;

app.Run();
