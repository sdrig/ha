﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Text;
using Prometheus;
namespace be2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IConfigurationRoot _configuration;
        private readonly IDatabaseAsync _databaseAsync;
        private readonly IDistributedCache _distributedCache;
        public WeatherForecastController(ILogger<WeatherForecastController> logger,
            IConfigurationRoot configuration,
             IConnectionMultiplexer connectionMultiplexer,
            IDistributedCache distributedCache)
        {
            _logger = logger;
            _configuration = configuration;
            _databaseAsync = connectionMultiplexer.GetDatabase();
            _distributedCache = distributedCache;   
        }

      
        [HttpGet]
        [Route("w/{city}")]
        public async Task<string> Test(string city)
        {
            try
            {
                var value = await _databaseAsync.StringGetAsync(city);
                return value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}